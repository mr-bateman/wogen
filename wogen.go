package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"
)


func letter_in_arr(letter byte, arr []byte) bool {
	for _, v := range arr {
		if v == letter {
			return true
		}
	}
	return false
}

func main() {
	reader := bufio.NewReader(os.Stdin)
	input := ""

	if len(os.Args) < 2 {
		input, _ = reader.ReadString('\n')
	} else {
		input = os.Args[1]
	}
	input = strings.Trim(input, "\n ")


	var base_time = time.Now().UnixNano()
	var Alphabet = [26]byte{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
				'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
				'w', 'x', 'y', 'z',}
	var vowels = [5]byte{'a', 'e', 'i', 'o', 'u'}

	var consonants = [21]byte{'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z',}

	var length, _ = strconv.Atoi(input)
	var word = make([]byte, 0, length)

	rand.Seed(time.Now().UnixNano() - base_time)
	letter := Alphabet[rand.Int() % len(Alphabet)]

	for i := 0; i < length; i++ {
		// new seed every iteration
		rand.Seed(time.Now().UnixNano() - base_time)
		if letter_in_arr(letter, vowels[:]) == true {
			word = append(word, letter)
			// next letter is a consonant
			letter = consonants[rand.Int() % len(consonants)]
		} else {
			if (letter == 's' || letter == 'c') && rand.Int() % 2 == 0 {
				// 'sh' 'ch' formations are possible
				word = append(word, letter)
				if i + 2 <= length {
					word = append(word, 'h')
					// compensate for 2 letters in one iteration
					i++
				}
			} else if rand.Int() % 21 == 0 {
				// double consonants are possible, such as 'daRK'
				word = append(word, letter)
				if i + 2 <= length {
					word = append(word, consonants[rand.Int() % len(consonants)])
					i++
				}
			} else {
				word = append(word, letter)
			}
			// next letter is a vowel
			letter = vowels[rand.Int() % len(vowels)]
		}
	}
	fmt.Println(string(word))
	// input, _ = reader.ReadString('\n') // if not running from term
}
